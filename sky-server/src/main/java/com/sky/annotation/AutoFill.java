package com.sky.annotation;

import com.sky.enumeration.OperationType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解:用于标识需要自动填充的字段
 */

@Target(ElementType.METHOD)  // 注解只能加在方法上，表示该方法需要自动填充
@Retention(RetentionPolicy.RUNTIME)  //固定写法
public @interface AutoFill {
    // 通过枚举的方式指定当前数据库操作类型
    // OperationType 自定义枚举类，用于标识数据库操作类型
    // OperationType.INSERT/UPDATE 表示当前方法是插入或更新操作
    OperationType value();
}


















